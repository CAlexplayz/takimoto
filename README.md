# Takimoto

## What is Takimoto?
Takimoto is a fun/social Discord bot with flexible roleplay functionality.  
  
Takimoto is written in node.js and currently in development.

![](https://vignette.wikia.nocookie.net/new-game/images/c/cf/44494149_2166121337047929_1638944101221007360_n.jpg/revision/latest?cb=20181025093321)

> Sojiro-kun, do you like it when I smile? - Hifumi Takimoto

## Todo
- [ ] Make a todo list

## How to build
You will first need to make a bot account. Instructions on how to can be found here: https://discordjs.guide/preparations/setting-up-a-bot-application.html  
  
Then after cloning the repository, enter the directory in your console and type `npm i(nstall)`  
  
Rename `sample.env` to just `.env` and enter your bot token where it says `[YOUR BOT TOKEN HERE]`  
(You can skip this step, but you will have to enter the token every time you start the bot)  
  
You can now start the bot with `node .` if you did the previous step or `BOT_TOKEN=[YOUR BOT TOKEN HERE] node .` if you didn't.  
  
If everything goes well, then your bot should now be online and running!
