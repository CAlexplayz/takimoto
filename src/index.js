require('dotenv').config()

const Discord = require('discord.js')
const client = new Discord.Client()

client.on('ready', () => {
	console.log(`Successfully logged in as ${client.user.tag}!`);
	client.user.setActivity('Sojiro 👀', { type: 'WATCHING' });
})

client.on('message', message => {
	if (message.mentions.users.get(client.user.id))
		message.react('👀')
})

client.login(process.env.BOT_TOKEN)
